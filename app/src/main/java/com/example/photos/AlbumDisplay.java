package com.example.photos;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.example.photos.model.Album;
import com.example.photos.model.Photo;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

public class AlbumDisplay extends AppCompatActivity {
    public static final String ALBUM_NAME = "albumName";
    public static final String ALBUM_INDEX = "albumIndex";
    public static final int ADD_PHOTO_CODE = 1;
    public static final int MOVE_PHOTO_CODE = 2;
    public static final int COPY_PHOTO_CODE = 3;
    public static final int PHOTO_DISPLAY_CODE = 4;
    private ListView listView;
    private Album album;
    private ArrayList<Album> albums = new ArrayList<>();
    private ArrayList<Photo> photos = new ArrayList<>();
    private String path;
    private int albumIndex;
    private int check;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_display);
        path = this.getApplicationInfo().dataDir + "/data.dat";
        Intent intent = getIntent();
        albums = (ArrayList<Album>) intent.getSerializableExtra("albums");
        albumIndex = intent.getIntExtra("albumIndex", 0);
        System.out.println("pos" + albumIndex);
        album = albums.get(albumIndex);
        photos = album.getPhotos();
        image = findViewById(R.id.image_view);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ArrayAdapter<Photo> adapter = new ArrayAdapter<>(this, R.layout.album, photos);
        adapter.setNotifyOnChange(true);
        listView = findViewById(R.id.album_display);
        listView.setAdapter(adapter);
        listView.setItemChecked(0, true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listView.setItemChecked(position, true);
                check = position;
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                listView.setItemChecked(position, true);
                check = position;
                openPhoto();
                return true;
            }
        });
    }

    public void openPhoto() {
        Intent intent = new Intent(this, PhotoDisplay.class);
        intent.putExtra("albums", albums);
        intent.putExtra("albumIndex", albumIndex);
        intent.putExtra("photoIndex", check);
        startActivityForResult(intent,PHOTO_DISPLAY_CODE);
    }

    public void addPhoto() {
        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
        } else {
            intent = new Intent(Intent.ACTION_GET_CONTENT);
        }
        intent.setType("image/*");
        startActivityForResult(intent, ADD_PHOTO_CODE);
    }

    public void deletePhoto() {
        if (photos.size() == 0) {
            new AlertDialog.Builder(this)
                    .setMessage("There are no photos.")
                    .setPositiveButton("OK", null)
                    .show();
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);


        builder.setMessage("Are you sure you want to perform this action?");
        builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        photos.remove(check);
                        Save.saveData(albums, path);
                        listView.setItemChecked(check, true);
                    }
                });
        builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.show();

        listView.setAdapter(new ArrayAdapter<>(this, R.layout.album, photos));
    }

    public void copyPhoto(){
        Intent intent = new Intent(this, CopyMovePhoto.class);
        intent.putExtra("albums", albums);
        intent.putExtra("albumIndex", albumIndex);
        intent.putExtra("photoIndex", check);
        startActivityForResult(intent, COPY_PHOTO_CODE);
    }

    public void movePhoto(){
        Intent intent = new Intent(this, CopyMovePhoto.class);
        intent.putExtra("albums", albums);
        intent.putExtra("albumIndex", albumIndex);
        intent.putExtra("photoIndex", check);
        startActivityForResult(intent, MOVE_PHOTO_CODE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.album_display_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_photo:
                addPhoto();
                return true;
            case R.id.action_delete_photo:
                deletePhoto();
                return true;
            case R.id.action_copy_photo:
                copyPhoto();
                return true;
            case R.id.action_move_photo:
                movePhoto();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode != RESULT_OK) {
            return;
        }


        if (requestCode == ADD_PHOTO_CODE) {
            Uri selectedImage = intent.getData();
            image.setImageURI(selectedImage);
            Bitmap bitmap = ((BitmapDrawable) image.getDrawable()).getBitmap();
            String name = selectedImage.getLastPathSegment();
            Photo photo = new Photo(name, bitmap);
            photos.add(photo);
            System.out.println(name);
            Save.saveData(albums, path);
        }
        if(requestCode ==COPY_PHOTO_CODE){
            albums=(ArrayList<Album>) intent.getSerializableExtra("albums");
            photos=albums.get(albumIndex).getPhotos();
            Save.saveData(albums, path);
            listView.setAdapter(new ArrayAdapter<>(this, R.layout.album, photos));
        }
        if(requestCode ==MOVE_PHOTO_CODE){
            albums=(ArrayList<Album>) intent.getSerializableExtra("albums");
            photos=albums.get(albumIndex).getPhotos();
            photos.remove(check);
            Save.saveData(albums, path);
            listView.setAdapter(new ArrayAdapter<>(this, R.layout.album, photos));
        }
        if(requestCode ==PHOTO_DISPLAY_CODE){
            albums = (ArrayList<Album>) intent.getSerializableExtra("albums");
            int i=intent.getIntExtra("albumIndex", 0);
            album=albums.get(i);
            photos=album.getPhotos();
            listView.setAdapter(new ArrayAdapter<>(this, R.layout.album, photos));
        }


        //listView.setAdapter(new ArrayAdapter<>(this, R.layout.album, photos));
        //ArrayAdapter<Photo> adapter = new ArrayAdapter<>(this, R.layout.album, photos);

        //redo adapter

    }

}
