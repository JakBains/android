package com.example.photos;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.EditText;

public class AddAlbum extends AppCompatActivity {

    public static final String ALBUM_NAME = "albumName";
    public static final String ALBUM_INDEX = "albumIndex";
    private EditText albumName;
    private int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_album);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        albumName = findViewById(R.id.album_name);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            index = bundle.getInt(ALBUM_INDEX);
            albumName.setText(bundle.getString(ALBUM_NAME));
        }
    }

    public void save(View view) {
        final String name = albumName.getText().toString();
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Are you sure you want to perform this action?");
        builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (name.isEmpty()||name.contains(" ")) {
                            new AlertDialog.Builder(builder.getContext())
                                    .setMessage("Name cannot be blank or contain spaces")
                                    .setPositiveButton("Ok", null)
                                    .show();
                            return;
                        }
                        Bundle bundle = new Bundle();
                        bundle.putInt(ALBUM_INDEX, index);
                        bundle.putString(ALBUM_NAME, name);

                        Intent intent = new Intent();
                        intent.putExtras(bundle);

                        setResult(RESULT_OK, intent);

                        finish();
                    }
                });
        builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.show();

    }

}