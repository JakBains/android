package com.example.photos;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.photos.model.Album;
import com.example.photos.model.Photo;
import com.example.photos.model.Tag;

import java.util.ArrayList;


public class PhotoDisplay extends AppCompatActivity {
    public static final String ALBUM_NAME = "albumName";
    public static final String ALBUM_INDEX = "albumIndex";
    public static final int ADD_PHOTO_CODE = 1;
    private ListView tagList;
    private Album album;
    private Tag tag;
    private int tagIndex;
    private EditText edit_text;
    private ArrayList<Album> albums = new ArrayList<>();
    private ArrayList<Photo> photos = new ArrayList<>();
    private ArrayList<Tag> tags = new ArrayList<>();
    private String path;
    private int albumIndex;
    private int photoIndex;
    private int check;
    private ImageView image;
    private Photo photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_display);
        path = this.getApplicationInfo().dataDir + "/data.dat";
        Intent intent = getIntent();
        albums = (ArrayList<Album>) intent.getSerializableExtra("albums");
        albumIndex = intent.getIntExtra("albumIndex", 0);
        photoIndex = intent.getIntExtra("photoIndex", 0);
        album = albums.get(albumIndex);
        photos = album.getPhotos();
        photo = photos.get(photoIndex);
        image = findViewById(R.id.image_display);
        image.setImageBitmap(photo.getBitmap());
        tags=photo.getTags();
        edit_text=findViewById(R.id.tag_text);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ArrayAdapter<Tag> adapter = new ArrayAdapter<>(this, R.layout.album, tags);
        adapter.setNotifyOnChange(true);
        tagList = findViewById(R.id.tagList);
        tagList.setAdapter(adapter);
        tagList.setItemChecked(0, true);

        tagList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tagList.setItemChecked(position, true);
                tagIndex=position;
            }
        });

        Spinner spinner = findViewById(R.id.tag_spinner);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.tag_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);

        tagList.setAdapter(new ArrayAdapter<>(this, R.layout.album, tags));

    }

    public void previousPhoto() {
        if (photoIndex > 0) {
            photoIndex--;
            Photo previous = photos.get(photoIndex);
            image.setImageBitmap(previous.getBitmap());
            ArrayAdapter<Tag> adapter=new ArrayAdapter<>(this,R.layout.album,previous.getTags());
            tagList.setAdapter(adapter);
            tagList.setItemChecked(0,true);
        }
    }
    public void nextPhoto() {
        if (photoIndex < photos.size()-1) {
            photoIndex++;
            Photo next = photos.get(photoIndex);
            image.setImageBitmap(next.getBitmap());
            ArrayAdapter<Tag> adapter=new ArrayAdapter<>(this,R.layout.album,next.getTags());
            tagList.setAdapter(adapter);
            tagList.setItemChecked(0,true);
        }
    }

    public void addTag(View view) {
        Spinner spinner = findViewById(R.id.tag_spinner);
        final String tagText = edit_text.getText().toString();
        final String tagType = spinner.getSelectedItem().toString();
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Are you sure you want to perform this action?");
        builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (tagText.isEmpty()||tagText.contains(" ")) {
                            new AlertDialog.Builder(builder.getContext())
                                    .setMessage("Tag cannot be blank or contain spaces")
                                    .setPositiveButton("Ok", null)
                                    .show();
                            return;
                        }

                        for (int i = 0; i < tags.size(); i++) {
                            if (tagText.equalsIgnoreCase(tags.get(i).getTagText()) && tagType.equalsIgnoreCase(tags.get(i).getTagType())) {
                                new AlertDialog.Builder(builder.getContext())
                                        .setMessage("This tag already exists!")
                                        .setPositiveButton("Ok", null)
                                        .show();
                                return;
                            }
                        }

                        tagList.setItemChecked(tagIndex, true);
                        tag = new Tag(tagType, tagText);
                        tags.add(tag);
                        Save.saveData(albums, path);
                    }
                });
        builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.show();

        /*
        ArrayAdapter<Tag> adapter = new ArrayAdapter<>(this, R.layout.album, tags);
        tagList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        */
        tagList.setAdapter(new ArrayAdapter<>(this, R.layout.album, tags));
    }

    public void deleteTag(View view) {
        if (tags.size() == 0) {
            new AlertDialog.Builder(this)
                    .setMessage("There are no tags.")
                    .setPositiveButton("OK", null)
                    .show();
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Are you sure you want to perform this action?");
        builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        tagList.setItemChecked(tagIndex, true);
                        tags.remove(tagIndex);
                        Save.saveData(albums, path);
                    }
                });
        builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.show();

        tagList.setAdapter(new ArrayAdapter<>(this, R.layout.album, tags));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent();
                intent.putExtra("albums", albums);
                intent.putExtra("albumIndex", albumIndex);
                setResult(RESULT_OK, intent);
                finish();
                return true;
            case R.id.action_previous:
                previousPhoto();
                return true;
            case R.id.action_next:
                nextPhoto();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.photo_display_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

}
