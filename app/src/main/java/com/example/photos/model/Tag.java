package com.example.photos.model;

import java.io.Serializable;

public class Tag implements Serializable {
    private static final long serialVersionUID = 1234567L;
    private String type;
    private String text;

    public Tag(String type, String text) {
        this.type = type;
        this.text = text;
    }

    @Override
    public String toString() {
        return "Tag: " + type + "\nType: " + text;
    }

    public String getTagType() {
        return type;
    }

    public String getTagText() {
        return text;
    }

    public void setTagType(String type){
        this.type=type;
    }

    public void setTagText(String text){
        this.text=text;
    }
}
