package com.example.photos.model;



import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.widget.ImageView;


import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Photo implements Serializable{

    private static final long serialVersionUID=12345L;
    private ArrayList<Tag> tags=new ArrayList<Tag>();
    private String name;
    private SerializedBitmap bitmap;

    public Photo(String name, Bitmap bitmap) {
        this.name = name;
        this.bitmap = new SerializedBitmap(bitmap);


    }
    public String toString(){
        return name;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public void addTag(Tag tag) {
        tags.add(tag);
    }
    public void deleteTag(Tag tag) {
        tags.remove(tag);
    }


    public Bitmap getBitmap() {
        return bitmap.getBitmap();
    }


    public boolean equals(Photo other) {
        return this.name.equals(other.name);
    }
}


class SerializedBitmap implements Serializable {
    private byte[] bytes;

    public SerializedBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        bytes = stream.toByteArray();
    }

    public Bitmap getBitmap() {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }
}
