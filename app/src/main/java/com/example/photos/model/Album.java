package com.example.photos.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Album implements Serializable {

        private static final long serialVersionUID = 1234L;
        String name;
        ArrayList<Photo> photos;

        public Album(String name) {
            this.name = name;
            photos = new ArrayList();
        }

        public String getName() {
            return this.name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public ArrayList<Photo> getPhotos() {
            return this.photos;
        }

        public int getPhotoCount() {
            return this.photos.size();
        }

        public boolean equals(Album other) {
            return name.equals(other.name);
        }

        @Override
        public String toString() {
            String result = "Name: " + name + "\nPhoto Count: " + photos.size() + "\n";
            return result;
        }

    }

