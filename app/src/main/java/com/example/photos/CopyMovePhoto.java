package com.example.photos;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;



import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.photos.model.Album;
import com.example.photos.model.Photo;

import java.util.ArrayList;

public class CopyMovePhoto extends AppCompatActivity {

    private ListView listView;
    private ArrayList<Album> albums = new ArrayList<>();
    private ArrayList<Album> albumsChoice = new ArrayList<>();
    private ArrayList<Photo> photos = new ArrayList<>();
    private String path;
    private int albumIndex;
    private int photoIndex;
    private int check;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.copy_move_photo);
        path = this.getApplicationInfo().dataDir + "/data.dat";
        Intent intent = getIntent();
        albums = (ArrayList<Album>) intent.getSerializableExtra("albums");
        albumIndex = intent.getIntExtra("albumIndex", 0);
        photoIndex = intent.getIntExtra("photoIndex", 0);
        photos=albums.get(albumIndex).getPhotos();

        //albumsChoice=albums;
        for(int x=0;x<albums.size();x++){
            albumsChoice.add(albums.get(x));
        }

        albumsChoice.remove(albumIndex);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ArrayAdapter<Album> adapter = new ArrayAdapter<>(this, R.layout.album, albumsChoice);
        adapter.setNotifyOnChange(true);
        listView = findViewById(R.id.albums_list);
        listView.setAdapter(adapter);
        listView.setItemChecked(0, true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listView.setItemChecked(position, true);
                check = position;
                copy();
            }
        });


    }
    public void copy(){


        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        Album album=albumsChoice.get(check);
        Photo photo=photos.get(photoIndex);
        int index=albums.indexOf(album);
        albums.get(index).getPhotos().add(photo);

        builder.setMessage("Are you sure you want to perform this action?");
        builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent();
                        intent.putExtra("albums", albums);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                });
        builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.show();

    }


}
