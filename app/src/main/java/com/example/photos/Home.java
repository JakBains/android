package com.example.photos;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.photos.model.Album;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;



public class Home extends AppCompatActivity {

    public static final int ADD_ALBUM_CODE = 1;
    public static final int RENAME_ALBUM_CODE = 2;

    private ArrayList<Album> albums = new ArrayList<>();
    private ListView listView;
    private String path;
    int check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.albums_list);
        path = this.getApplicationInfo().dataDir + "/data.dat";
        File data = new File(path);

        // if data.dat doesn't exist, create it
        if (!data.exists() || !data.isFile()) {
            try {
                data.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // data.dat exists, read it
        try {
            FileInputStream fileInput = new FileInputStream(path);
            ObjectInputStream objInput = new ObjectInputStream(fileInput);
            albums = (ArrayList<Album>) objInput.readObject();

            fileInput.close();
            objInput.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        ArrayAdapter<Album> adapter = new ArrayAdapter<>(this, R.layout.album, albums);
        adapter.setNotifyOnChange(true);
        listView = findViewById(R.id.albums_list);
        listView.setAdapter(adapter);
        listView.setItemChecked(0, true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listView.setItemChecked(position, true);
                check = position;
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                listView.setItemChecked(position, true);
                check = position;
                openAlbum();
                return true;
            }
        });
    }

    public void addAlbum() {
        Intent intent = new Intent(this, AddAlbum.class);
        startActivityForResult(intent, ADD_ALBUM_CODE);
    }

    public void renameAlbum() {
        if (albums.size() == 0) {
            new AlertDialog.Builder(this)
                    .setMessage("There are no albums.")
                    .setPositiveButton("OK", null)
                    .show();
            return;
        }
        Bundle bundle = new Bundle();
        Album album = albums.get(check);
        bundle.putString(AddAlbum.ALBUM_NAME, album.getName());
        bundle.putInt(AddAlbum.ALBUM_INDEX, check);

        Intent intent = new Intent(this, AddAlbum.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, RENAME_ALBUM_CODE);
    }

    public void deleteAlbum() {

        if (albums.size() == 0) {
            new AlertDialog.Builder(this)
                    .setMessage("There are no albums.")
                    .setPositiveButton("OK", null)
                    .show();
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Are you sure you want to perform this action?");
        builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        albums.remove(check);
                        Save.saveData(albums, path);
                        listView.setItemChecked(check, true);
                    }
                });
        builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.show();

        listView.setAdapter(new ArrayAdapter<>(this, R.layout.album, albums));
    }

    public void openAlbum() {
        Intent intent = new Intent(this, AlbumDisplay.class);
        intent.putExtra("albums", albums);
        intent.putExtra("albumIndex", check);
        startActivity(intent);
    }

    public void search(){
        Intent intent = new Intent(this, Search.class);
        intent.putExtra("albums", albums);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                addAlbum();
                return true;
            case R.id.action_delete:
                deleteAlbum();
                return true;
            case R.id.action_rename:
                renameAlbum();
                return true;
            case R.id.action_search:
                search();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode != RESULT_OK) {
            return;
        }

        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            return;
        }

        String name = bundle.getString(AddAlbum.ALBUM_NAME);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        for (int i = 0; i < albums.size(); i++) {
            if (name.equalsIgnoreCase(albums.get(i).getName())) {
                new AlertDialog.Builder(builder.getContext())
                        .setMessage("This album already exists!")
                        .setPositiveButton("Ok", null)
                        .show();
                return;
            }
        }

        if (requestCode == ADD_ALBUM_CODE) {
            albums.add(new Album(name));
            Save.saveData(albums, path);
        }
        if (requestCode == RENAME_ALBUM_CODE) {
            Album rename = albums.get(check);
            rename.setName(name);
            Save.saveData(albums, path);
        }


        //redo adapter
        listView.setAdapter(new ArrayAdapter<>(this, R.layout.album, albums));
    }


    // Creates menu buttons using home_menu.xml
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }



}
