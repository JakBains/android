package com.example.photos;

import android.widget.ArrayAdapter;

import com.example.photos.model.Album;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class Save {

    public static void saveData(ArrayList<Album> albums, String path) {
        try {
            FileOutputStream fileOutput = new FileOutputStream(path);
            ObjectOutputStream objOutput = new ObjectOutputStream(fileOutput);
            objOutput.writeObject(albums);

            objOutput.close();
            fileOutput.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveData(ArrayAdapter<Album> arrayAdapter, String path) {
        ArrayList<Album> albums = new ArrayList<>();
        for (int index = 0; index < arrayAdapter.getCount(); index++) {
            albums.add(arrayAdapter.getItem(index));
        }

        try {
            FileOutputStream fileOutput = new FileOutputStream(path);
            ObjectOutputStream objOutput = new ObjectOutputStream(fileOutput);
            objOutput.writeObject(albums);

            objOutput.close();
            fileOutput.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}



