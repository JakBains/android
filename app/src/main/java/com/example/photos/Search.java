package com.example.photos;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.example.photos.model.Album;
import com.example.photos.model.Photo;
import com.example.photos.model.Tag;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;

public class Search extends AppCompatActivity {
    private ListView photoView;
    private ListView tagView;
    private ArrayList<Album> albums = new ArrayList<>();
    private ArrayList<Photo> photos = new ArrayList<>();
    private ArrayList<Photo> allPhotos = new ArrayList<>();
    private ArrayList<Tag> tags = new ArrayList<>();
    private String path;
    private boolean tog;
    private EditText edit_text;
    private Tag tag;
    private int tagIndex;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);
        path = this.getApplicationInfo().dataDir + "/data.dat";
        Intent intent = getIntent();
        albums = (ArrayList<Album>) intent.getSerializableExtra("albums");
        edit_text=findViewById(R.id.tag_text);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        for(Album i:albums){
            for(Photo n:i.getPhotos()){
            allPhotos.add(n);
            }
        }


        ArrayAdapter<Tag> tagAdapter = new ArrayAdapter<>(this, R.layout.album, tags);
        tagAdapter.setNotifyOnChange(true);
        tagView = findViewById(R.id.tagView);
        tagView.setAdapter(tagAdapter);
        tagView.setItemChecked(0, true);
        tagView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tagView.setItemChecked(tagIndex, true);


            }
        });


        ArrayAdapter<Photo> photoAdapter = new ArrayAdapter<>(this, R.layout.album, photos);
        photoAdapter.setNotifyOnChange(true);
        photoView = findViewById(R.id.photoView);
        photoView.setAdapter(photoAdapter);
        photoView.setItemChecked(0, true);
        photoView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                photoView.setItemChecked(position, true);


            }
        });
        Spinner spinner = findViewById(R.id.tag_spinner);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.tag_array, android.R.layout.simple_spinner_item);
        tagAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
    }

    public void addTag(View view){
        Spinner spinner = findViewById(R.id.tag_spinner);
        final String tagText = edit_text.getText().toString();
        final String tagType = spinner.getSelectedItem().toString();
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Are you sure you want to perform this action?");
        builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (tagText.isEmpty()||tagText.contains(" ")) {
                            new AlertDialog.Builder(builder.getContext())
                                    .setMessage("Tag cannot be blank or contain spaces")
                                    .setPositiveButton("Ok", null)
                                    .show();
                            return;
                        }

                        for (int i = 0; i < tags.size(); i++) {
                            if (tagText.equalsIgnoreCase(tags.get(i).getTagText()) && tagType.equalsIgnoreCase(tags.get(i).getTagType())){
                                new AlertDialog.Builder(builder.getContext())
                                        .setMessage("This tag already exists!")
                                        .setPositiveButton("Ok", null)
                                        .show();
                                return;
                            }
                        }

                        tagView.setItemChecked(tagIndex, true);
                        tag = new Tag(tagType, tagText);
                        tags.add(tag);
                    }
                });
        builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.show();

        tagView.setAdapter(new ArrayAdapter<>(this, R.layout.album, tags));
    }

    public void deleteTag(View view) {
        if (tags.size() == 0) {
            new AlertDialog.Builder(this)
                    .setMessage("There are no tags.")
                    .setPositiveButton("OK", null)
                    .show();
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Are you sure you want to perform this action?");
        builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        tagView.setItemChecked(tagIndex, true);
                        tags.remove(tagIndex);
                        Save.saveData(albums, path);
                    }
                });
        builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.show();

        tagView.setAdapter(new ArrayAdapter<>(this, R.layout.album, tags));
    }

    public void tog(View view){
        if(tog)
            tog=false;
        else
            tog=true;
    }



    public void searchPhotos(View view){
    photos= new ArrayList<Photo>();
        if (tog) {
            and();
        } else {
            or();
        }
    photoView.setAdapter(new ArrayAdapter<>(this, R.layout.album, photos));;
    }

    private void or() {

        for (Photo j : allPhotos) {
            outerloop:
            for (Tag k : j.getTags()) {
                for (Tag i : tags) {
                    if ((i.getTagType().equalsIgnoreCase(k.getTagType())
                            && i.getTagText().equalsIgnoreCase(k.getTagText()))||(i.getTagType().equalsIgnoreCase(k.getTagType())&&k.getTagText().contains(i.getTagText()))) {
                        photos.add(j);
                        break outerloop;
                    }
                }
            }
        }

    }

    private void and() {
        for (Photo j : allPhotos) {
            outerloop:
            for (Tag k : j.getTags()) {
                for (Tag i : tags) {
                    if ((!(i.getTagType().equalsIgnoreCase(k.getTagType()))
                            || !(i.getTagText().equalsIgnoreCase(k.getTagText())))||(!(i.getTagType().equalsIgnoreCase(k.getTagType()))
                            || !(i.getTagText().contains(k.getTagText())))) {
                        break outerloop;
                    }
                    photos.add(j);
                }
            }
        }
    }



}
